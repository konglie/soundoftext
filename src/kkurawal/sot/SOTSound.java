package kkurawal.sot;

import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.swing.*;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Konglie on 7/30/2016.
 */
public class SOTSound {
	public static boolean SoundPlayed = false;

	private final String text;
	private final SOTLang lang;
	private final String urlID;
	private String mp3Path;

	public SOTSound (String text, SOTLang lang, String urlID){
		this.text = text;
		this.lang = lang;
		this.urlID = urlID;
		fetchMp3File();
	}

	private void fetchMp3File(){
		// get the mp3 file url
		new Thread(new Runnable() {
			@Override
			public void run() {
				String url = SoundOfText.mp3URL(getUrlID());
				try {
					Document page = Jsoup.connect(url).get();
					mp3Path = page.select("audio > source").attr("src");
				} catch (IOException e) {
					e.printStackTrace();
					mp3Path = null;
				}
			}
		}).start();
	}

	@Override
	public String toString(){
		return String.format("\"%s\" in %s", text, lang.toString());
	}

	public String getText() {
		return text;
	}

	public SOTLang getLang() {
		return lang;
	}

	public String getUrlID() {
		return urlID;
	}

	public void playSound(){
		if(mp3Path == null || !mp3Path.toLowerCase().endsWith(".mp3")){
			fetchMp3File();
			JOptionPane.showMessageDialog(null, "MP3 Not ready, try again later.");
			return;
		}

		if(!SoundPlayed) {
			new JFXPanel();
		}
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				Media media = new Media(String.format("%s%s", SoundOfText.WebURL, mp3Path));
				MediaPlayer mp = new MediaPlayer(media);
				mp.play();
			}
		});
	}

	public boolean saveSound(String target){
		try {
			byte[] data = Jsoup.connect(String.format("%s/%s", SoundOfText.WebURL, mp3Path))
					.ignoreContentType(true)
					.execute()
					.bodyAsBytes();
			FileOutputStream fos = new FileOutputStream(target);
			fos.write(data);
			fos.close();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return  false;
		}
	}

}
