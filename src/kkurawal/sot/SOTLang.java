package kkurawal.sot;

/**
 * Created by Konglie on 7/30/2016.
 */
public class SOTLang {
	private String code, name;
	public SOTLang(String code, String name){
		this.code = code;
		this.name = name;
	}

	@Override
	public String toString(){
		return name;
	}

	public String getCode(){
		return code;
	}
}
