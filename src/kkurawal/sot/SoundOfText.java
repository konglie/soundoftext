package kkurawal.sot;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;

/**
 * Created by Konglie on 7/30/2016.
 */
public class SoundOfText {
	public static final String WebURL       = "http://soundoftext.com/";
	public static final String SoundSource  = "http://soundoftext.com/sounds";

	private Runnable pageLoadedRun                  = null;
	private Document thePage                        = null;
	private boolean pageLoaded                      = false;
	private LinkedHashMap<String, String> languages = new LinkedHashMap<>();

	public SoundOfText(Runnable onLoaded){
		this.pageLoadedRun = onLoaded;
	}

	public boolean isPageLoaded(){
		return pageLoaded && thePage != null;
	}

	public void loadPage(){
		try {
			thePage = Jsoup.connect(WebURL).get();
			pageLoaded = true;

			parseLanguages();
		} catch (Exception e) {
			e.printStackTrace();
			thePage = null;
			pageLoaded = false;
		}

		if(pageLoadedRun != null){
			pageLoadedRun.run();
		}
	}

	public LinkedHashMap<String, String> getLanguages(){
		return languages;
	}

	private void parseLanguages(){
		if(!isPageLoaded()){
			return;
		}

		languages.clear();
		Elements langs = thePage.select("select[name='lang'] > option");
		for(Element option : langs){
			languages.put(option.attr("value"), option.text());
		}
	}

	public SOTSound getTextSound(String text, SOTLang lang){
		HashMap<String, String> data = new HashMap<>();
		data.put("text", text);
		data.put("lang", lang.getCode());

		try {
			String json = Jsoup
					.connect(SoundSource)
					.data(data)
					.ignoreContentType(true)
					.post()
					.body()
					.text();

			String id = getIDFromJSON(json);
			if(id == null){
				return null;
			}
			return new SOTSound(text, lang, id);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	// of course,
	// should be using "proper" JSON Library
	public String getIDFromJSON(String json){

		// expected format
		// { "id": 531, "success": true }

		if(json.toLowerCase().indexOf("true") < 0){
			return null;
		}

		// quick and dirty
		// again, use a "proper" JSON Library
		return json.split(",")[0].split(":")[1].trim();
	}

	public static String mp3URL(String id){
		return String.format("%s/%s", SoundSource, id);
	}
}
