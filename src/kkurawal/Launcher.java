package kkurawal;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Created by Konglie on 7/30/2016.
 */
public class Launcher {
	public static void main(String[] args){
		try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch ( Exception e){

		}
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new SOTFrame().setVisible(true);
			}
		});
	}

	private static JFileChooser fc = null;
	public static String pickMP3Target(String title){
		FileFilter filter = new FileNameExtensionFilter("MP3 Files", new String[]{"mp3"});
		return pickFile(title, filter);
	}

	public static String pickFile(String title, FileFilter filter){
		if(fc == null){
			fc = new JFileChooser();
		}
		String res = null;
		if(filter != null)
		{
			fc.removeChoosableFileFilter(fc.getFileFilter());
			fc.setFileFilter(filter);
		}

		fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
		if(fc.showDialog(null, title) == JFileChooser.CANCEL_OPTION)
			return res;

		res = fc.getSelectedFile().getAbsolutePath();
		return res;
	}
}
