package kkurawal;

import kkurawal.sot.SOTLang;
import kkurawal.sot.SOTSound;
import kkurawal.sot.SoundOfText;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.LinkedHashMap;

import static java.awt.Color.LIGHT_GRAY;

/**
 * Created by Konglie on 7/30/2016.
 */
public class SOTFrame extends JFrame implements Runnable {
	static final Color TheGreen = new Color(0x008B8B);
	static final Border Padding = BorderFactory.createEmptyBorder(7,7,7,7);
	private SOTMainPanel mainPanel;

	private SoundOfText sot;
	private SOTFrame self;
	private JPanel results;

	public SOTFrame() {
		super("Sound of Text");
		self = this;
		self.sot = new SoundOfText(self);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(360, 640));
		setResizable(false);
		buildGUI();
		pack();
		setLocationRelativeTo(null);
		mainPanel.submit.setEnabled(false);

		new Thread(new Runnable() {
			@Override
			public void run() {
				self.sot.loadPage();
			}
		}).start();

		mainPanel.textField.setText("How're you doing man?");
	}

	private void buildGUI(){
		((JComponent)getContentPane()).setBorder(Padding);
		getContentPane().setLayout(new BorderLayout(0, 7));
		getContentPane().setBackground(TheGreen);

		mainPanel = new SOTMainPanel();
		getContentPane().add(mainPanel, BorderLayout.PAGE_START);

		mainPanel.submit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				mainPanel.submit.setText("Wait...");
				mainPanel.submit.setEnabled(false);
				new Thread(new Runnable() {
					@Override
					public void run() {
						String text             = mainPanel.textField.getText().trim();
						SOTLang lang            = (SOTLang) mainPanel.lang.getSelectedItem();
						final SOTSound sound    = self.sot.getTextSound(text, lang);
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								renderResult(sound);
								mainPanel.submit.setText("Submit");
								mainPanel.submit.setEnabled(true);
							}
						});
					}
				}).start();
			}
		});

		results = new JPanel(new MigLayout("insets 5, wrap 1, fillx"));
		results.setBackground(Color.WHITE);
		JScrollPane scrollPane = new JScrollPane(results);
		getContentPane().add(scrollPane, BorderLayout.CENTER);

		JLabel footer = new JLabel("<html>" +
				"<center>" +
				"Packed by konglie@kurungkurawal.com<br/>" +
				"This is using the awesome of " + SoundOfText.WebURL +
				"</center>" +
				"</html>");
		footer.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		footer.setOpaque(true);
		footer.setBackground(Color.WHITE);
		footer.setHorizontalAlignment(SwingConstants.CENTER);
		getContentPane().add(footer, BorderLayout.SOUTH);
	}

	private void renderResult(final SOTSound sound){
		if(results.getComponentCount() < 1){
			JLabel title = new JLabel("Results");
			title.setForeground(TheGreen);
			title.setFont(getFont().deriveFont(24f));
			title.setHorizontalAlignment(SwingConstants.CENTER);
			title.setBorder(Padding);
			results.add(title, "wrap, center");
		}

		JPanel res = new JPanel(new MigLayout("insets 10 0 5 0, fillx"));
		res.setBorder(BorderFactory.createMatteBorder(1,0,0,0, LIGHT_GRAY));
		res.setOpaque(false);

		JPanel texts = new JPanel(new MigLayout("insets 0, wrap 1"));
		texts.setOpaque(false);
		String text = sound.getText();
		if(text.length() > 35){
			text = text.substring(0, 35) + "...";
		}
		texts.add(new JLabel(text));

		JLabel langLbl = new JLabel(sound.getLang().toString());
		langLbl.setForeground(Color.LIGHT_GRAY.darker().darker().darker());
		texts.add(langLbl);

		JPanel btns = new JPanel(new MigLayout("insets 0"));
		btns.setOpaque(false);
		JButton play = new JButton("Play");
		play.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				sound.playSound();
			}
		});

		JButton save = new JButton("Save");
		save.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String target = Launcher.pickMP3Target("Save MP3 File");
				if(target != null){
					if(!target.toLowerCase().endsWith(".mp3")){
						target = target + ".mp3";
					}
					if(!sound.saveSound(target)) {
						JOptionPane.showMessageDialog(self, "Save Failed");
						return;
					}
					try {
						File t = new File(target);
						Desktop.getDesktop().open(new File(t.getParent()));
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		});
		btns.add(play);
		btns.add(save);

		res.add(texts, "left");
		res.add(btns, "right, push");
		results.add(res, "wrap, grow", 1);
	}

	@Override
	public void run() {
		if(sot == null || !sot.isPageLoaded()){
			return;
		}

		LinkedHashMap<String, String> langs = sot.getLanguages();
		for(String lang : langs.keySet()){
			mainPanel.lang.addItem(new SOTLang(lang, langs.get(lang)));
		}
		mainPanel.submit.setEnabled(true);
	}

	private class SOTMainPanel extends JPanel{
		JTextField textField;
		JComboBox lang;
		JButton submit;
		public SOTMainPanel() {
			super();
			setBorder(Padding);
			setBackground(Color.WHITE);
			setLayout(new MigLayout("inset 5, fill, wrap 1"));

			JLabel title = new JLabel("Sound Of Text");
			title.setForeground(TheGreen);
			title.setFont(getFont().deriveFont(24f));
			title.setHorizontalAlignment(SwingConstants.CENTER);
			title.setBorder(Padding);
			add(title, "wrap, center");

			add(label("Text:"));
			textField = new JTextField();
			textField.setBorder(
					BorderFactory.createCompoundBorder(
							BorderFactory.createLineBorder(TheGreen, 1),
							Padding
					)
			);
			add(textField, "grow");

			add(label("Language"));
			lang = new JComboBox();
			lang.setBorder(Padding);
			add(lang, "grow");

			submit = new JButton("Submit");
			submit.setBorder(Padding);
			add(submit, "grow");
		}

		JLabel label(String s){
			JLabel lbl = new JLabel(s);
			lbl.setOpaque(false);
			lbl.setBorder(Padding);
			return lbl;
		}
	}
}
